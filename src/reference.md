# Reference

```
Usage: csvs [options]

Manage csvs databases.

Options:
  -v, --version               output the version number
  -i, --source-path <string>  Path to source (default: "/Users/fetsorn/mm/codes/csvs-nodejs")
  -o, --target-path <string>  Path to target
  -t, --target-type <string>  Type of target (default: "json")
  -q, --query <string>        Search string (default: "?")
  -h, --help                  display help for command
```
