# Merge CSVS Datasets

```shell
csvs -i /path/to/source-dataset -o /path/to/target-dataset
```

For a complete list of cli commands and flags, see [Reference](./reference.md). Learn more about csvs in the other [User Guides](./user_guides.md).
