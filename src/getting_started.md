# Getting Started

Install from npm
```shell
yarn install -g --dev csvs
```

Or run in a nix shell
```shell
nix shell https://gitlab.com/norcivilian-labs/csvs-nodejs
```

To create a new csvs dataset
```shell
csvs -o /path/to/csvs-dataset
```

To print all entries in a dataset to stdout
```shell
csvs -i /path/to/csvs-dataset
```

To print stats for a dataset
```shell
csvs --stats -i /path/to/csvs-dataset
```

To search for entries with a date that starts in 2005
```shell
csvs -i /path/to/csvs-dataset -q "?date=2005.*"
```

For a complete list of cli commands and flags, see [Reference](./reference.md). Learn more about csvs in the [Tutorial](./tutorial.md) and [User Guides](./user_guides.md).
