# Import Filesystem Data

```shell
csvs -i /path/to/source-folder -o /path/to/target-dataset
```
For a complete list of cli commands and flags, see [Reference](./reference.md). Learn more about csvs in the other [User Guides](./user_guides.md).
