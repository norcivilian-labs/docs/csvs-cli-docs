# Map CSVS Schemas

The recommended way to merge csvs datasets with different schemas is by using jq filters.

```shell
csvs -i /path/to/source-dataset | jq '.' | -o /path/to/target-dataset
```

For a complete list of cli commands and flags, see [Reference](./reference.md). Learn more about csvs in the other [User Guides](./user_guides.md).
