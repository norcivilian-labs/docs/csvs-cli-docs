# Design

client for csvs, data converter

competes: prsql, sqlite

interacts: with filesystem

constitutes: a cli shell application, spawned system processes

includes: cli scaffold, stream pipe, import streams for csvs, jsonlines, vk, tg; export streams for csvs, biorg, tex, jsonlines

patterns: 

resembles: jq

stakeholders: fetsorn
