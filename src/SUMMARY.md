# Summary

- [Getting Started](./getting_started.md)
- [Tutorial](./tutorial.md)
- [User Guides](./user_guides.md)
  - [Import from Filesystem](./01_import_from_filesystem.md)
  - [Import from Telegram](./02_import_from_telegram.md)
  - [Import from VKontakte](./03_import_from_vkontakte.md)
  - [Merge CSVS Datasets](./04_merge_csvs_datasets.md)
  - [Edit CSVS Schema](./05_edit_csvs_schema.md)
  - [Map CSVS schemas](./06_map_csvs_schemas.md)
- [Reference](./reference.md)
- [Design](./design.md)
- [Requirements](./requirements.md)
